﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class AssaultRifleShoot : NetworkBehaviour {

	//TODO: Need to make this generic for other weapons.

	GameObject ch; //what is ch??? Is this PlayerWeapon?
	Animation gunAnim;
	AudioSource gunSound;
	bool isFiring = false;
	bool isADS = false;
	// Use this for initialization
	void Start () {
		gunAnim = GetComponent<Animation> ();
		gunAnim.Play ("Draw");
		ch = GameObject.FindGameObjectWithTag ("Crosshair");

		gunSound = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {
		if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0))) {
			isFiring = true;
		} else if ((Input.GetButtonUp ("Fire1") || Input.GetMouseButtonUp (0))) {
			isFiring = false;
		}
		
		List<string> gunAnimNames = new List<string> ();
		foreach (AnimationState state in gunAnim) {
			gunAnimNames.Add (state.name);
		}
		if (Input.GetKeyDown(KeyCode.R) && GlobalAmmo.currentExtraAmmo > 0 && GlobalAmmo.currentAmmo < GlobalAmmo.maxClipSize) {
			//AudioSource reloadSound = GetComponent<AudioSource> ();
			//gunSound.Play ();
			Reload();
		}

		if(Input.GetButtonDown("Fire2")){
			gunAnim.Play ("ADS");
			isADS = true;
			ch.SetActive (false);
		}
		if(Input.GetButtonUp("Fire2")){
			//gunAnim.Play ("Take001");
			isADS = false;
			gunAnim.Play ("DrawReverse");
			ch.SetActive (true);
		}



		/*if (isFiring) {
			
				if (GlobalAmmo.currentAmmo > 0) {
					
					if (isADS) {
						if (!gunAnim.IsPlaying ("ADSFire")) { 
							GlobalAmmo.currentAmmo--;
							gunSound.Play ();
							gunAnim.Play ("ADSFire");
						}
					} else {
						if (!gunAnim.IsPlaying ("Fire")) {
							GlobalAmmo.currentAmmo--;
							gunSound.Play ();
							gunAnim.Play ("Fire");
						}
					}

				}
			}
			*/


		if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown(0)) && GlobalAmmo.currentAmmo > 0) {
			AudioSource gunSound = GetComponent<AudioSource> ();
			//gunSound.Play ();
			gunAnim.Stop ();
			if (isADS) {
				gunAnim.Play ("ADSFire");
			} else {
				gunAnim.Play ("Fire");
			}
			gunSound.Play ();
			GlobalAmmo.currentAmmo--;
		}

	}
	public void Reload(){
		GlobalAmmo.currentAmmo = GlobalAmmo.currentExtraAmmo + GlobalAmmo.currentAmmo;

		if(GlobalAmmo.currentAmmo >= GlobalAmmo.maxClipSize) {
			gunAnim.Play ("Reload");
			GlobalAmmo.currentExtraAmmo = GlobalAmmo.currentAmmo - GlobalAmmo.maxClipSize;
			GlobalAmmo.currentAmmo = GlobalAmmo.maxClipSize;
		}
	}
}
