﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tower {
	public string type;
	public GameObject prefab;
	public int price = 200;
	public Vector3 pos = Vector3.zero;
	public List<Vector3> nodes = new List<Vector3>();

	public Tower(string type, GameObject prefab, int price, Vector3 pos){
		this.type = type;
		this.prefab = prefab;
		this.price = price;
		this.pos = pos;
	}
	public Tower(GameObject prefab, int price){
		this.prefab = prefab;
		this.price = price;
	}
	public Tower(Tower tower, Vector3 pos){
		this.prefab = tower.prefab;
		this.pos = tower.pos;
	}
	public Tower (GameObject prefab,  Vector3 pos){
		this.prefab = prefab;
		this.pos = pos;
	}

	public bool SameType(Tower t2){
		if (type == t2.type) {
			return true;
		}
		return false;
	}
	public bool SameType(string t2){
		if (type == t2) {
			return true;
		}
		return false;
	}
}

public class GridInteraction : MonoBehaviour {
	Tower towerSlot;
	GameObject towerPanel;
	Camera mainCamera;
	GameObject wireBox;
	Player player;
	public Material wireBoxMat;
	public bool gridInteractionEnabled;
	public List<GameObject> towerPrefabs;
	List<Tower> towerTypes;
	List<Tower> instantiatedTowers;
	List<Vector3> currentNodes;
	int selectedTowerTypeIndex = 0;
	private Tower _currentTower;
	public Tower currentTower {
		get { 
			_currentTower = towerTypes [selectedTowerTypeIndex];
			return _currentTower;
		}
		set { _currentTower = value; }
	}
	Vector3 instanPos = Vector3.zero;
	RaycastHit hit;

	void TowerSetup(){
		towerTypes = new List<Tower>();
		instantiatedTowers = new List<Tower>();
		towerTypes.Add (new Tower("Trigun", towerPrefabs[0], 200, instanPos));
	}
	// Use this for initialization
	void Start () {
		//temp way of adding prices to towers
		currentNodes = new List<Vector3>();
		TowerSetup();
		player = GetComponent<Player> ();
		gridInteractionEnabled = false;
		wireBox = GameObject.CreatePrimitive (PrimitiveType.Cube);
		wireBox.GetComponent<MeshRenderer> ().material = wireBoxMat;
		wireBox.GetComponent<BoxCollider> ().enabled = false;
		wireBox.SetActive (false);
		wireBox.transform.localScale = new Vector3 (Grid.nodeRadius * 4, .3f, Grid.nodeRadius * 4);
	}
		

	// Update is called once per frame
	void Update () {
		if (GameManager.state != GameManager.GameState.PHASE_BUILDING) {
			gridInteractionEnabled = false;
			wireBox.SetActive (false);
			return;
		}
		if (towerSlot != null) {
			towerSlot.prefab.transform.position = mainCamera.ScreenPointToRay (Input.mousePosition).direction * 4;
		}
		mainCamera = FindCamera();
		if (Input.anyKeyDown) {
			GetInput ();
		}
		if (gridInteractionEnabled) {

			hit = new RaycastHit (); 
			// We need to actually hit an object
			if (
				!Physics.Raycast (mainCamera.ScreenPointToRay (Input.mousePosition).origin,
					mainCamera.ScreenPointToRay (Input.mousePosition).direction, out hit, 4,
					Physics.DefaultRaycastLayers)) {
				wireBox.SetActive (false);
				return;
			}
			// Check that we are hitting a gameobject with tag Ground
			if (hit.collider.gameObject.tag != "Ground") {
				wireBox.SetActive (false);
				return;
			}
			//If inside Grid
			if (Grid.GetNode (hit.point) != null) {
				instanPos = GetMiddleOfNodes (hit.point);
				wireBox.SetActive (true);
				wireBox.transform.position = instanPos;
				currentNodes = GetGridNodes (instanPos, Grid.nodeRadius);
			}
		} else {
			if (wireBox.activeSelf) {
				wireBox.SetActive (false);
			}
		}
	}
	private Camera FindCamera()
	{
		if (GetComponent<Camera>())
		{
			return GetComponent<Camera>();
		}

		return Camera.main;
	}
	private List<Vector3> GetGridNodes(Vector3 _instanPos, float nodeRadius){
		List<Vector3> nodes = new List<Vector3> ();
		Vector3 nodePos = Vector3.zero;

		nodePos.x = _instanPos.x + nodeRadius;
		nodePos.z = _instanPos.z + nodeRadius;
		nodes.Add (nodePos);
		nodePos.x = _instanPos.x + nodeRadius;
		nodePos.z = _instanPos.z - nodeRadius;
		nodes.Add (nodePos);
		nodePos.x = _instanPos.x - nodeRadius;
		nodePos.z = _instanPos.z + nodeRadius;
		nodes.Add (nodePos);
		nodePos.x = _instanPos.x - nodeRadius;
		nodePos.z = _instanPos.z - nodeRadius;
		nodes.Add (nodePos);

		return nodes;
	}
	private Vector3 GetMiddleOfNodes(Vector3 hit){
		Debug.Log ("1: " + instanPos);
		GridNode currentNode = Grid.GetNode (hit);
		Vector3 offset = hit - currentNode.worldPos;
		Vector3 middle = currentNode.worldPos;
		if (offset.x >= 0 && offset.z >= 0) {
			middle.x += Grid.nodeRadius;
			middle.z += Grid.nodeRadius;
		}else if (offset.x >= 0 && offset.z < 0) {
			middle.x += Grid.nodeRadius;
			middle.z -= Grid.nodeRadius;
		}else if (offset.x < 0 && offset.z >= 0) {
			middle.x -= Grid.nodeRadius;
			middle.z += Grid.nodeRadius;
		}else if (offset.x < 0 && offset.z < 0) {
			middle.x -= Grid.nodeRadius;
			middle.z -= Grid.nodeRadius;
		}
		Debug.Log ("2: " + instanPos);

		return middle;
	}
	private void PlaceTower(List<Vector3> nodes, Tower tower, Vector3 instanPos){
		if (!IsWalkableNodes (nodes)) {
			Debug.Log ("Cannot place tower here as one or more nodes are unwalkable");
			return;	
		}
		if (tower.price > player.funds) {
			Debug.Log ("Cannot place tower as not enough funds");
			return;
		}

		Instantiate (tower.prefab, instanPos, Quaternion.identity);
		instantiatedTowers.Add (tower);
		SetUnwalkableNodes (nodes);
		tower.nodes = nodes;
		player.funds -= tower.price;
	}
	private bool IsWalkableNodes(List<Vector3> nodes){
		foreach (Vector3 node in nodes) {
			if (!Grid.GetNode (node).walkable) {
				return false;
			}
		}
		return true;
	}
	private void SetUnwalkableNodes(List<Vector3> nodes){
		foreach (Vector3 node in nodes) {
			GridNode gn = Grid.GetNode (node);
			gn.walkable = false;
		}
	}
	private void GetInput(){
		
		if (Input.GetKeyDown (KeyCode.I)) {
			gridInteractionEnabled = !gridInteractionEnabled;
			wireBox.SetActive (!wireBox.activeSelf);
		}
		if(gridInteractionEnabled){
			//select tower type
			for (int i = 0; i < towerTypes.Count - 1; i++) {
				if (Input.GetKeyDown (i.ToString())) {
					selectedTowerTypeIndex = i;
				}
			}
			if (Input.GetMouseButtonDown (0) && wireBox.activeSelf) {
				Debug.Log ("Ping" + IsWalkableNodes(currentNodes));
				if (IsWalkableNodes(currentNodes) && player.funds >= towerTypes[selectedTowerTypeIndex].price) {
					if (towerTypes[selectedTowerTypeIndex].SameType("Trigun")) {
						PlaceTower (currentNodes, towerTypes [selectedTowerTypeIndex], instanPos);

					}	
				}
			}

			if (Input.GetMouseButtonDown (1)) {
				if (Physics.Raycast (mainCamera.ScreenPointToRay (Input.mousePosition).origin,
					mainCamera.ScreenPointToRay (Input.mousePosition).direction, out hit, 4,
					Physics.DefaultRaycastLayers)) {
					if (hit.collider.gameObject.tag == "Tower") {
						foreach (Tower tower in instantiatedTowers) {
							if (tower.prefab == hit.collider.gameObject) {
								player.funds += tower.price / 2;
								foreach (Vector3 tn in tower.nodes) {
									Grid.GetNode (tn).walkable = true;
								}
								Destroy (tower.prefab);
								instantiatedTowers.Remove (tower);
							}
						}
					}
				}
			}
			if (Input.GetKeyDown (KeyCode.Z)) {
				if (towerSlot == null) {
					if (hit.collider.gameObject.tag == "Tower") {
						foreach (Tower tower in instantiatedTowers) {
							if (tower.prefab == hit.collider.gameObject) {
								Debug.Log ("Tower");
								tower.prefab.GetComponent<BoxCollider> ().enabled = false;
								towerSlot = tower;
								Destroy (tower.prefab);
								instantiatedTowers.Remove (tower);
							}
						}
					}
				}else {
					if (Grid.GetNode (hit.point) != null) {
						PlaceTower (currentNodes, towerSlot, GetMiddleOfNodes (hit.point));
					}
				}
			}
		}
	}
}
